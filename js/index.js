
$(function(){
   $("[data-toggle='tooltip']").tooltip();
   $("[data-toggle='popover']").popover();
   $(".carousel").carousel({
     interval : 2000
   });

   $('#contacto').on('show.bs.modal', function (e) {
       console.log('el modal contacto se está mostrando');         
       $('#contactoBtn').removeClass('btn-contacto'); 
       $('#contactoBtn').prop('disabled', true); 
   });
   $('#contacto').on('shown.bs.modal', function (e) {
       console.log('el modal contacto se mnostró');
   });
   $('#contacto').on('hide.bs.modal', function (e) {
       console.log('el modal contacto se oculta');
   });
   $('#contacto').on('hidden.bs.modal', function (e) {         
       console.log('el modal contacto se ocultó');            
       $('#contactoBtn').addClass('btn-contacto');            
       $('#contactoBtn').prop('disabled', false); 
   });
});
