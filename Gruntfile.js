module.exports = function (grunt) {
    grunt.inittConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        watch: {
            files: ['css/*.scss'],
            task: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: { //browser files                    
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                }
            },
            options: {
                watchTask: true,
                server: {
                    baseDir: './' //Directorio base para nuestro servidor
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{ //browser files                    
                    expand: true,
                    cwd: './',
                    src: 'image/*.{png.gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        }
    });
    grunt.loadNpmTask('grunt-contrib-watch');
    grunt.loadNpmTask('grunt-contrib-sass');
    grunt.loadNpmTask('grunt-bower-sync');
    grunt.loadNpmTask('grunt-contrib-imagemin');
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
};